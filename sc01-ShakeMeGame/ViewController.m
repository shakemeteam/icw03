//
//  ViewController.m
//  sc01-ShakeMeGame
//
//  Created by user on 10/2/17.
//  Copyright © 2017 user. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    NSTimer *timer;
    int     counter;
    int     score;
    int     rotateScore;
    int     gameMode; // 1 - Being Played
                      // 2 - Game Over
    
}
@property (weak, nonatomic) IBOutlet UIButton *btnStart;
@property (weak, nonatomic) IBOutlet UILabel *lblTimer;
@property (weak, nonatomic) IBOutlet UILabel *lblScore;
@property (weak, nonatomic) IBOutlet UILabel *lblRotate;
@property (weak, nonatomic) IBOutlet UISlider *sldTimerSet;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    rotateScore = 0;
    score = 0;
    counter = 10;

    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)startGamePressed:(id)sender {
    if (score == 0 || gameMode == 2){
        gameMode = 1; // we started the game
        self.lblTimer.text = [NSString stringWithFormat:@"%i Seconds", counter];
        // create a timer
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(startCounter) userInfo:nil repeats:YES];
        self.btnStart.enabled = NO;
        
    }
    
    if (counter == 0){
        rotateScore = 0;
        score = 0;
        counter = 10;
        
        self.lblTimer.text = [NSString stringWithFormat:@"%i Seconds", counter];
        self.lblScore.text = [NSString stringWithFormat:@"%i Shakes", score];
        self.lblRotate.text = [NSString stringWithFormat:@"%i Rotations", rotateScore];
        
    }
    

    
}

-(void)startCounter {
    // decrement the counter
    self.sldTimerSet.enabled = NO;
    counter -= 1;
    self.lblTimer.text = [NSString stringWithFormat:@"%i Seconds", counter];

    
    if (counter == 0){
        [timer invalidate];
        gameMode = 2;  // game is over
        
        // toggle the start button to "Restart", and make it enabled
        [self.btnStart setTitle:@"Restart" forState:UIControlStateNormal];
        self.btnStart.enabled = YES;
        self.sldTimerSet.enabled = YES;
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Game Over"
                                                                       message:[NSString stringWithFormat:@"Your Total is: %i", score + rotateScore]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    

    
}

-(void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event{
    if (event.subtype == UIEventSubtypeMotionShake){
        if(gameMode == 1){
            // increment the score
            score += 1;
            
            // display
            self.lblScore.text = [NSString stringWithFormat:@"%i Shakes", score];
            
        }
        

    }

    
    
}

-(void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    if(gameMode == 1){
        rotateScore += 1;
        
        self.lblRotate.text = [NSString stringWithFormat:@"%i Rotations", rotateScore];
    }
}


- (IBAction)setTimer:(id)sender {
        self.lblTimer.text = [NSString stringWithFormat:@"%i Seconds", (int)self.sldTimerSet.value];
        counter = (int)self.sldTimerSet.value;
    
}




@end
